using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Data;
using UrbanPeople.Models;

namespace UrbanPeople.Tests
{
    public class DatabaseFixture : IDisposable
    {
        public const string UserEmail = "test@test.com";
        public const string UserPassword = "123456";

        public readonly List<Tag> Tags = new List<Tag>
        {
            new Tag("Театры"),
            new Tag("Кино"),
            new Tag("Бары"),
            new Tag("Рестораны"),
            new Tag("Фестивали"),
        };

        public User User = new User {Email = UserEmail};

        public SqliteConnection Connection { get; }

        public ApplicationDbContext Context { get; }

        public DatabaseFixture()
        {
            Connection = new SqliteConnection("DataSource = :memory:");
            Connection.Open();

            Context = new ApplicationDbContext(GetContextOptions());
            Context.Database.EnsureCreated();
        }

        public async Task CreateAsync()
        {
            await CreateRolesAsync();
            await CreateUserAsync();
            await CreateTagsAsync();
        }

        private DbContextOptions<ApplicationDbContext> GetContextOptions()
        {
            return new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite(Connection)
                .Options;
        }

        public ApplicationDbContext CreateDbContext()
        {
            return new ApplicationDbContext(GetContextOptions());
        }

        private async Task CreateRolesAsync()
        {
            var roleStore = new RoleStore<Role>(Context);

            var roleManager = new RoleManager<Role>(
                roleStore,
                null,
                null,
                null,
                null
            );
                    
            string[] roles = { Role.Admin, Role.Developer, Role.Partner, Role.Client};
            foreach (string role in roles)
            {
                await roleManager.CreateAsync(new Role(role));
            }
        }

        private async Task CreateUserAsync()
        {
            var userManager = TestSuite.CreateUserManager(Context);

            await userManager.CreateAsync(User, UserPassword);
            await userManager.AddToRoleAsync(User, Role.Client);
        }

        private async Task CreateTagsAsync()
        {
            Context.AddRange(Tags);
            await Context.SaveChangesAsync();
        }
        
        public void Dispose()
        {
            Connection.Close();
        }
    }
}