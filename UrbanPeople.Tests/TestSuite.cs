using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using UrbanPeople.Data;
using UrbanPeople.Models;

namespace UrbanPeople.Tests
{
    public static class TestSuite
    {
        /// <summary>
        /// Подготавливает UserManager 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static UserManager<User> CreateUserManager(ApplicationDbContext context)
        {
            var userStore = new UserStore<User, Role, ApplicationDbContext>(context);
            return new UserManager<User>(
                userStore,
                null,
                new PasswordHasher<User>(),
                null,
                null,
                null,
                null,
                null,
                null
            );
        }
    }
}