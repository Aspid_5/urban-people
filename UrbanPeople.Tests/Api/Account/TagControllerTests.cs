using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UrbanPeople.Areas.Api.Controllers.Account;
using UrbanPeople.Areas.Api.Models.Dto;
using UrbanPeople.Models;
using UrbanPeople.Services;
using Xunit;

namespace UrbanPeople.Tests.Api.Account
{
    public class TagControllerTests
    {
        private readonly DatabaseFixture _fixture;

        public TagControllerTests()
        {
            _fixture = new DatabaseFixture();
        }

        [Fact]
        public async void AddTags_SingleRequest_Success()
        {
            await _fixture.CreateAsync();
            
            var userManager = TestSuite.CreateUserManager(_fixture.Context);
            var userTagService = new UserTagService(_fixture.Context);

            var controller = new TagController(_fixture.Context, userManager, userTagService)
            {
                ControllerContext =
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = new ClaimsPrincipal(
                            new ClaimsIdentity(
                                new List<Claim> {new Claim(ClaimTypes.Name, DatabaseFixture.UserEmail)}
                            )
                        )
                    }
                }
            };

            var request = _fixture.Tags
                .Select(tag => tag.Id)
                .Take(3)
                .ToList();

            var response = await controller.AddTags(request);

            var createdAtAction = Assert.IsType<CreatedAtActionResult>(response);
            var userId = Assert.IsType<string>(createdAtAction.Value);
            Assert.NotEmpty(userId);
        }

        [Fact]
        public async void GetTags_SingleRequest_Success()
        {
            await _fixture.CreateAsync();
            
            var userManager = TestSuite.CreateUserManager(_fixture.Context);
            var userTagService = new UserTagService(_fixture.Context);

            var user = await userManager.FindByEmailAsync(DatabaseFixture.UserEmail);
            Assert.IsType<User>(user);

            var controller = new TagController(_fixture.Context, userManager, userTagService)
            {
                ControllerContext =
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = new ClaimsPrincipal(
                            new ClaimsIdentity(
                                new List<Claim> {new Claim(ClaimTypes.Name, DatabaseFixture.UserEmail)}
                            )
                        )
                    }
                }
            };

            var response = await controller.GetTags(user.Id);

            var list = Assert.IsType<ActionResult<List<TagDto>>>(response);
            Assert.Empty(list.Value);
        }
    }
}