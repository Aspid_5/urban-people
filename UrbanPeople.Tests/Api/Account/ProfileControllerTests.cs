using Microsoft.AspNetCore.Mvc;
using UrbanPeople.Areas.Api.Controllers.Account;
using UrbanPeople.Areas.Api.Models;
using UrbanPeople.Areas.Api.Models.Dto;
using Xunit;

namespace UrbanPeople.Tests.Api.Account
{
    public class ProfileControllerTests
    {
        private readonly DatabaseFixture _fixture;

        public ProfileControllerTests()
        {
            _fixture = new DatabaseFixture();
        }

        [Fact]
        public async void Registration_SingleRequest_Success()
        {
            await _fixture.CreateAsync();

            const string userEmail = "info@test.com";
            const string password = "123456";
            
            var controller = new ProfileController(TestSuite.CreateUserManager(_fixture.Context));

            var result = await controller.Registration(userEmail, password);

            var createdAtAction = Assert.IsType<CreatedAtActionResult>(result);
            var profileDto = Assert.IsType<ProfileDto>(createdAtAction.Value);

            Assert.NotEmpty(profileDto.Id);
            Assert.IsType<string>(profileDto.Id);
            Assert.Equal(userEmail, profileDto.Email);
        }

        [Fact]
        public async void Registration_SingleRequest_Fail()
        {
            await _fixture.CreateAsync();

            const string userEmail = "test@test.com";
            const string password = "";
            
            var controller = new ProfileController(TestSuite.CreateUserManager(_fixture.Context));
            
            var result = await controller.Registration(userEmail, password);

            var badRequest = Assert.IsType<BadRequestObjectResult>(result);
            Assert.IsType<BadRequestError>(badRequest.Value);
        }
    }
}