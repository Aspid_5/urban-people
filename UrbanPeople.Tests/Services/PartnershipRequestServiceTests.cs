using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Data;
using UrbanPeople.Models;
using UrbanPeople.Services;
using Xunit;

namespace UrbanPeople.Tests.Services
{
    public class PartnershipRequestServiceTests
    {
        [Fact]
        public async void CreateRequestAsync_SingleRequest_Success()
        {
            var connection = PrepareConnection();

            try
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite(connection)
                    .Options;

                const string email = "info@rainbow.com";
                const string name = "RAINBOW LTD";
                const string description = "Описание партнера";

                using (var context = new ApplicationDbContext(options))
                {
                    context.Database.EnsureCreated();
                    var service = new PartnershipRequestService(context);
                    await service.CreateRequestAsync(email, name, description);
                }

                using (var context = new ApplicationDbContext(options))
                {
                    var request = await context.PartnershipRequests.FirstOrDefaultAsync();

                    Assert.IsType<PartnershipRequest>(request);
                    Assert.Equal(email, request.Email);
                    Assert.Equal(name, request.Name);
                    Assert.Equal(description, request.Description);
                }
            }
            finally
            {
                connection.Close();
            }
        }

        private static SqliteConnection PrepareConnection()
        {
            var connection = new SqliteConnection("DataSource = :memory:");
            connection.Open();
            return connection;
        }
    }
}