using System.Linq;
using UrbanPeople.Services;
using Xunit;

namespace UrbanPeople.Tests.Services
{
    public class UserTagServiceTests
    {
        private readonly DatabaseFixture _fixture;

        private readonly UserTagService _userTagService;

        public UserTagServiceTests()
        {
            _fixture = new DatabaseFixture();
            _userTagService = new UserTagService(_fixture.Context);
        }

        [Fact]
        public async void AddTags_SingleRequest_Success()
        {
            await _fixture.CreateAsync();

            var number = await _userTagService.AddTagsAsync(_fixture.User, _fixture.Tags.Take(4).ToList());
            Assert.Equal(4, number);
            Assert.Equal(4, _fixture.CreateDbContext().UserTags.Count());
        }

        [Fact]
        public async void AddTags_MultipleRequests_Success()
        {
            await _fixture.CreateAsync();

            // Добавляем три первых тега
            var number = await _userTagService.AddTagsAsync(_fixture.User, _fixture.Tags.Take(3).ToList());
            Assert.Equal(3, number);
            Assert.Equal(3, _fixture.CreateDbContext().UserTags.Count());

            // Добавляем четвертый тэг
            number = await _userTagService.AddTagsAsync(_fixture.User, _fixture.Tags.Skip(3).Take(1).ToList());
            Assert.Equal(1, number);
            Assert.Equal(4, _fixture.CreateDbContext().UserTags.Count());

            // Добавляем пустой список
            number = await _userTagService.AddTagsAsync(_fixture.User, _fixture.Tags.Take(0).ToList());
            Assert.Equal(0, number);
            Assert.Equal(4, _fixture.CreateDbContext().UserTags.Count());
        }
    }
}