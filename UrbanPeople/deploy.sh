#!/usr/bin/env bash

tagname=$1

runtime_name="urbanpeople"
backup_name="urbanpeople-backup"

if [[ -z "$tagname" ]]; then
    echo "Укажите версию релиза в формате x.y.z"
    exit 1
fi

echo -e "\n=> Сборка образа"
docker build -t nativecodellc/urbanpeople:$tagname .
docker push nativecodellc/urbanpeople:$tagname

export DOCKER_HOST="ssh://root@urbanpeople"

echo -e "\n=> Получение образа"
docker login -u nativecodellc -p "b4e0c218-9afc-4319-a0ab-603c95de66f0"
docker pull nativecodellc/urbanpeople:$tagname

echo -e "\n=> Проверка запущенных контейнеров"
container=$(docker ps -a --format="{{.Names}}" --filter="name=$runtime_name")
if [[ "$container" -eq "urban" ]]; then
    echo -e "\n=> Подготовка бэкапа"
    docker rm "$backup_name"
    docker rename "$runtime_name" "$backup_name"
    
    status=$(docker inspect -f '{{.State.Running}}' $backup_name)
    if [[ "$status" -eq "true" ]]; then
        echo -e "\n=> Остановка предыдущей версии приложения"
        docker stop "$backup_name"
    fi
fi

echo -e "\n=> Запуск новой версии приложения"
docker run -d\
    --name=$runtime_name\
    --network=host\
    --log-driver=fluentd\
    --log-opt tag="docker.urbanpeople"\
    --restart=on-failure:5\
    nativecodellc/urbanpeople:$tagname

container=$(docker ps --format="{{.Names}}" --filter="name=$runtime_name")
if [[ "$container" -eq "true" ]]; then
    echo -e "\n=> Приложение запущено"
fi

unset DOCKER_HOST

exit 0
