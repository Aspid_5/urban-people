﻿﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
 using UrbanPeople.Data.Configurations;
 using UrbanPeople.Models;

namespace UrbanPeople.Data
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, string>
    {
        public new DbSet<User> Users { get; set; }

        public new DbSet<Role> Roles { get; set; }

        public DbSet<PartnershipRequest> PartnershipRequests { get; set; }

        public DbSet<Point> Points { get; set; }

        public DbSet<Route> Routes { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<UserTag> UserTags { get; set; }

        public DbSet<RouteTag> RouteTags { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new PointConfiguration());
            builder.ApplyConfiguration(new RouteConfiguration());
            builder.ApplyConfiguration(new UserTagConfiguration());
            builder.ApplyConfiguration(new RouteTagConfiguration());
        }
    }
}