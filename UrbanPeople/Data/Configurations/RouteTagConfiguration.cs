using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UrbanPeople.Models;

namespace UrbanPeople.Data.Configurations
{
    public class RouteTagConfiguration : IEntityTypeConfiguration<RouteTag>
    {
        public void Configure(EntityTypeBuilder<RouteTag> builder)
        {
            builder
                .HasKey(routeTag => new {routeTag.RouteId, routeTag.TagId});

            builder
                .HasOne(routeTag => routeTag.Route)
                .WithMany(route => route.RouteTags)
                .HasForeignKey(routeTag => routeTag.RouteId);

            builder
                .HasOne(routeTag => routeTag.Tag)
                .WithMany(tag => tag.RouteTags)
                .HasForeignKey(routeTag => routeTag.TagId);
        }
    }
}