using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UrbanPeople.Models;

namespace UrbanPeople.Data.Configurations
{
    public class UserTagConfiguration : IEntityTypeConfiguration<UserTag>
    {
        public void Configure(EntityTypeBuilder<UserTag> builder)
        {
            builder
                .HasKey(userTag => new {userTag.UserId, userTag.TagId});

            builder
                .HasOne(userTag => userTag.User)
                .WithMany(user => user.UserTags)
                .HasForeignKey(userTag => userTag.UserId);

            builder
                .HasOne(userTag => userTag.Tag)
                .WithMany(tag => tag.UserTags)
                .HasForeignKey(userTag => userTag.TagId);
        }
    }
}