using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UrbanPeople.Models;

namespace UrbanPeople.Authentication
{
    public class XAuthTokenHandler : AuthenticationHandler<XAuthTokenOptions>
    {
        private const string AuthHeaderName = "X-Auth-Token";

        public const string TokenProvider = "UrbanPeople";

        public const string TokenPurpose = "XAuthToken";

        private readonly UserManager<User> _userManager;

        public XAuthTokenHandler(
            IOptionsMonitor<XAuthTokenOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            UserManager<User> userManager)
            : base(options, logger, encoder, clock)
        {
            _userManager = userManager;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey(AuthHeaderName))
            {
                return AuthenticateResult.NoResult();
            }

            string headerValue;

            try
            {
                var headerBytes = Convert.FromBase64String(Request.Headers[AuthHeaderName]);
                headerValue = Encoding.UTF8.GetString(headerBytes);
            }
            catch (SystemException)
            {
                return AuthenticateResult.Fail($"Invalid header {AuthHeaderName}.");
            }

            string[] parts = headerValue.Split(":");

            if (parts.Length != 2)
            {
                return AuthenticateResult.Fail($"Invalid header {AuthHeaderName}.");
            }

            var userId = parts[0].Trim();
            var token = parts[1].Trim();

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return AuthenticateResult.Fail($"Invalid user {userId}.");
            }
            
            if (await _userManager.VerifyUserTokenAsync(user, TokenProvider, TokenPurpose, token))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.AuthenticationMethod, Scheme.Name)
                };

                var identity  = new ClaimsIdentity(claims, Scheme.Name);
                var principal = new ClaimsPrincipal(identity);
                
                return AuthenticateResult.Success(new AuthenticationTicket(principal, Scheme.Name)); 
            }

            return AuthenticateResult.Fail($"Invalid token {token}.");
        }
    }
}