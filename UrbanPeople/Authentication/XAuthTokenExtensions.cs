using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace UrbanPeople.Authentication
{
    public static class XAuthTokenExtensions
    {
        public static AuthenticationBuilder AddXAuthToken(this AuthenticationBuilder builder)
        {
            return AddXAuthToken(builder, XAuthTokenDefaults.AuthenticationScheme, _ => { });
        }

        public static AuthenticationBuilder AddXAuthToken(this AuthenticationBuilder builder, string authenticationScheme)
        {
            return AddXAuthToken(builder, authenticationScheme, _ => { });
        }

        public static AuthenticationBuilder AddXAuthToken(this AuthenticationBuilder builder, Action<XAuthTokenOptions> configureOptions)
        {
            return AddXAuthToken(builder, XAuthTokenDefaults.AuthenticationScheme, configureOptions);
        }

        public static AuthenticationBuilder AddXAuthToken(this AuthenticationBuilder builder, string authenticationScheme, Action<XAuthTokenOptions> configureOptions)
        {
            builder.Services.AddSingleton<IPostConfigureOptions<XAuthTokenOptions>, XAuthTokenPostConfigureOptions>();

            return builder.AddScheme<XAuthTokenOptions, XAuthTokenHandler>(authenticationScheme, configureOptions);
        }
    }
}