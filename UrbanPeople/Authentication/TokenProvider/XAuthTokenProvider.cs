﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace UrbanPeople.Authentication.TokenProvider
{
    public class XAuthTokenProvider<TUser> : DataProtectorTokenProvider<TUser> 
        where TUser : class
    {
        public XAuthTokenProvider(IDataProtectionProvider dataProtectionProvider, IOptions<DataProtectionTokenProviderOptions> options)
            : base(dataProtectionProvider, options)
        {
        }
    }
}