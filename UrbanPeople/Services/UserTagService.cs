using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrbanPeople.Data;
using UrbanPeople.Models;
using UrbanPeople.Services.Exceptions;

namespace UrbanPeople.Services
{
    public class UserTagService
    {
        /// <summary>
        /// Максимальное количество тэгов на пользователя
        /// </summary>
        public const int MaxTags = 10;

        private readonly ApplicationDbContext _context;

        public UserTagService(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Добавление предпочтений к пользователю user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="tags"></param>
        /// <returns>Количество добавленных тэгов</returns>
        public async Task<int> AddTagsAsync(User user, List<Tag> tags)
        {
            var existsTags = user.UserTags.Select(ut => ut.Tag).ToList();
            var newTags = tags.Except(existsTags).ToList();

            if (existsTags.Count + newTags.Count > MaxTags)
            {
                throw new InvalidArgumentException();
            }

            _context.UserTags.AddRange(newTags.Select(newTag => new UserTag(user, newTag)));

            return await _context.SaveChangesAsync();
        }
    }
}