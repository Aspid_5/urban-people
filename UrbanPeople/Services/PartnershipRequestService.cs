﻿using System.Threading.Tasks;
using UrbanPeople.Data;
using UrbanPeople.Models;

namespace UrbanPeople.Services
{
    public class PartnershipRequestService
    {
        private readonly ApplicationDbContext _context;

        public PartnershipRequestService(ApplicationDbContext _context)
        {
            this._context = _context;
        }

        public async Task<PartnershipRequest> CreateRequestAsync(string email, string name, string description)
        {
            var partnershipRequest = new PartnershipRequest(email, name, description);

            _context.PartnershipRequests.Add(partnershipRequest);

            await _context.SaveChangesAsync();

            return partnershipRequest;
        }
    }
}
