using System.Collections.Generic;

namespace UrbanPeople.Models
{
    public class Tag
    {
        public string Id { get; set; }
        
        public string Title { get; set; }

        public Tag(string title)
        {
            Title = title;
        }

        public ICollection<UserTag> UserTags { get; set; }
        
        public ICollection<RouteTag> RouteTags { get; set; }
    }
}