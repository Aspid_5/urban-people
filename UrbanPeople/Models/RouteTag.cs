namespace UrbanPeople.Models
{
    public class RouteTag
    {
        public string RouteId { get; set; }

        public Route Route { get; set; }

        public string TagId { get; set; }

        public Tag Tag { get; set; }
    }
}