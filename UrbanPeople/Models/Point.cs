using System;

namespace UrbanPeople.Models
{
    /// <summary>
    /// Точка маршрута
    /// </summary>
    public class Point
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string RouteId { get; set; }

        /// <summary>
        /// Маршрут
        /// </summary>
        public Route Route { get; set; }

        private double _latitude;

        /// <summary>
        /// Широта
        /// </summary>
        public double Latitude
        {
            get => _latitude;
            set => _latitude = value >= -90 && value <= 90
                ? value
                : throw new ArgumentException("Широта может принимать значения в диапазоне [-90; 90]");
        }

        private double _longitude;

        /// <summary>
        /// Долгота
        /// </summary>
        public double Longitude
        {
            get => _longitude;
            set => _longitude = value >= - 180 && value <= 180
                ? value
                : throw new ArgumentException("Долгота может принимать значения в диапазоне [-180; 180]");
        }

        /// <summary>
        /// Порядковый номер в маршруте
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Имя (необязательно)
        /// </summary>
        public string Name { get; set; }

        public Point()
        {
        }

        public Point(double latitude, double longitude, string name = null)
        {
            _latitude = latitude;
            _longitude = longitude;
            Name = name;
        }
    }
}