using System.Collections.Generic;

namespace UrbanPeople.Models
{
    /// <summary>
    /// Маршрут
    /// </summary>
    public class Route
    {
        public string Id { get; set; }

        public string OwnerId { get; set; }

        public User Owner { get; set; }

        public ICollection<Point> Points { get; set; }

        public string Name { get; set; }

        public ICollection<RouteTag> RouteTags { get; set; }

        public Route()
        {
            Points = new List<Point>();
            RouteTags = new List<RouteTag>();
        }

        public Route(User owner, string name) : this()
        {
            Owner = owner;
            Name = name;
        }

        /// <summary>
        /// Добавление точки в конец маршрута
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public Route AddPoint(Point point)
        {
            point.Number = Points.Count + 1;
            Points.Add(point);
            return this;
        }
    }
}