using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace UrbanPeople.Models
{
    public class User : IdentityUser
    {
        public ICollection<UserTag> UserTags { get; set; }

        public User()
        {
            UserTags = new List<UserTag>();
        }

        public User(string userName) : base(userName)
        {
            UserTags = new List<UserTag>();
        }
    }
}