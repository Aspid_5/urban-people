using Microsoft.AspNetCore.Identity;

namespace UrbanPeople.Models
{
    public class Role : IdentityRole
    {
        public const string Admin = "Admin";
        public const string Developer = "Developer";
        public const string Partner = "Partner";
        public const string Client = "Client";

        public Role()
        {
        }

        public Role(string name) : base(name)
        {
        }
    }
}