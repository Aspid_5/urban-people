﻿using System;

namespace UrbanPeople.Models
{
    public class PartnershipRequest
    {
        public const int New = 1;

        public const int Rejected = 2;

        public const int Approved = 3;

        public PartnershipRequest(string email, string name, string description)
        {
            Email = email ?? throw new ArgumentNullException(nameof(email));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Description = description ?? throw new ArgumentNullException(nameof(description));
            Status = New;
            CreatedAt = DateTime.Now;
            ModifiedAt = DateTime.Now;
        }

        public string Id { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ModifiedAt { get; set; }
    }
}
