namespace UrbanPeople.Models
{
    public class UserTag
    {
        public string UserId { get; set; }

        public User User { get; set; }

        public string TagId { get; set; }

        public Tag Tag { get; set; }

        public UserTag()
        {
        }

        public UserTag(User user, Tag tag)
        {
            User = user;
            Tag = tag;
        }
    }
}