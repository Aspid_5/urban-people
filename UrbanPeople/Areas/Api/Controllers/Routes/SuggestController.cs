using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Areas.Api.Models.Dto;
using UrbanPeople.Authentication;
using UrbanPeople.Data;

namespace UrbanPeople.Areas.Api.Controllers.Routes
{
    [ApiController, Area("Api")]
    [Route("/api/routes/suggest")]
    public class SuggestController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SuggestController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = XAuthTokenDefaults.AuthenticationScheme)]
        public async Task<ActionResult<RouteDto>> SuggestRoute()
        {
            var route = await _context.Routes
                .Include(model => model.Points)
                .FirstOrDefaultAsync();

            var result = new RouteDto
            {
                Id = route.Id,
                Name = route.Name,
                OwnerId = route.OwnerId,
                Points = new List<PointDto>(route.Points.Select(point => new PointDto
                {
                    Id = point.Id,
                    Latitude = point.Latitude,
                    Longitude = point.Longitude,
                    Name = point.Name,
                    Number = point.Number
                }))
            };

            return Ok(result);
        }
    }
}