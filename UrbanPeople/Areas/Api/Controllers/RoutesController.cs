using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Areas.Api.Models.Dto;
using UrbanPeople.Authentication;
using UrbanPeople.Data;

namespace UrbanPeople.Areas.Api.Controllers
{
    [ApiController, Area("Api")]
    [Route("/api/routes")]
    public class RoutesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public RoutesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = XAuthTokenDefaults.AuthenticationScheme)]
        public async Task<ActionResult<List<RouteDto>>> GetRoutes(int limit = 10, int offset = 0)
        {
            var list = await _context.Routes
                .Skip(offset)
                .Take(limit)
                .Include(route => route.Points)
                .AsNoTracking()
                .ToListAsync();

            var result = new List<RouteDto>(list.Select(route => new RouteDto
                {
                    Id = route.Id,
                    Name = route.Name,
                    OwnerId = route.OwnerId,
                    Points = new List<PointDto>(route.Points.Select(point => new PointDto
                    {
                        Id = point.Id,
                        Latitude = point.Latitude,
                        Longitude = point.Longitude,
                        Name = point.Name,
                        Number = point.Number
                    }))
                }
            ));

            return Ok(result);
        }
    }
}