using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Areas.Api.Models;
using UrbanPeople.Areas.Api.Models.Dto;
using UrbanPeople.Authentication;
using UrbanPeople.Data;
using UrbanPeople.Models;
using UrbanPeople.Services;

namespace UrbanPeople.Areas.Api.Controllers.Account
{
    [ApiController, Area("Api")]
    [Route("/api/account/tag")]
    public class TagController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        private readonly UserManager<User> _userManager;

        private readonly UserTagService _userTagService;

        public TagController(ApplicationDbContext context, UserManager<User> userManager, UserTagService userTagService)
        {
            _context = context;
            _userManager = userManager;
            _userTagService = userTagService;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = XAuthTokenDefaults.AuthenticationScheme)]
        public async Task<ActionResult<List<TagDto>>> GetTags(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return NotFound();
            }

            return user.UserTags
                .Select(userTag => new TagDto(userTag.Tag))
                .ToList();
        }

        /// <summary>
        /// Добавление предпочтений пользователя
        /// </summary>
        /// <param name="ids">Список id тегов</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = XAuthTokenDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AddTags(List<string> ids)
        {
            if (ids.Count == 0)
            {
                return BadRequest(new BadRequestError {Description = "Список тэгов пуст", Code = 100});
            }

            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            if (user == null)
            {
                return NotFound();
            }

            var tags = await _context.Tags
                .Where(tag => ids.Contains(tag.Id))
                .ToListAsync();

            await _userTagService.AddTagsAsync(user, tags);

            return CreatedAtAction(nameof(GetTags), user.Id);
        }
    }
}