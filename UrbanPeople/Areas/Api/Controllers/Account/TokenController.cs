using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UrbanPeople.Authentication;
using UrbanPeople.Models;

namespace UrbanPeople.Areas.Api.Controllers.Account
{
    [ApiController, Area("Api")]
    [Route("/api/account/token")]
    public class TokenController : ControllerBase
    {
        private readonly UserManager<User> _userManager;

        private readonly ILogger<TokenController> _logger;

        public TokenController(UserManager<User> userManager, ILogger<TokenController> logger)
        {
            _userManager = userManager;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult> GetAccessToken(string login, string password)
        {
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                return BadRequest();
            }
            
            var user = await _userManager.FindByEmailAsync(login);
            if (user == null)
            {
                return Unauthorized();
            }

            var verification = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
            if (verification == PasswordVerificationResult.Success)
            {
                var token = await _userManager.GenerateUserTokenAsync(user, XAuthTokenHandler.TokenProvider,
                    XAuthTokenHandler.TokenPurpose);

                await _userManager.SetAuthenticationTokenAsync(user, XAuthTokenHandler.TokenProvider,
                    XAuthTokenHandler.TokenPurpose, token);

                return Ok(BuildEncodedToken(user, token));
            }

            return Unauthorized();
        }

        private static TokenDto BuildEncodedToken(User user, string token)
        {
            return new TokenDto { EncodedToken = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{user.Id} : {token}"))};
        }
        
        private class TokenDto
        {
            public string EncodedToken { get; set; }
        }
    }
}