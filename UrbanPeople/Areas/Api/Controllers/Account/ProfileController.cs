using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Areas.Api.Models;
using UrbanPeople.Areas.Api.Models.Dto;
using UrbanPeople.Authentication;
using UrbanPeople.Models;

namespace UrbanPeople.Areas.Api.Controllers.Account
{
    [ApiController, Area("Api")]
    [Route("/api/account/profile")]
    public class ProfileController : ControllerBase
    {
        private readonly UserManager<User> _userManager;

        public ProfileController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Получение пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = XAuthTokenDefaults.AuthenticationScheme)]
        public async Task<ActionResult<ProfileDto>> GetProfile(string id = null)
        {
            if (string.IsNullOrEmpty(id))
            {
                // Возвращаем текущего пользователя.
                return Ok(new ProfileDto { Email = User.Identity.Name });
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(new ProfileDto { Email = user.UserName });
        }
        
        /// <summary>
        /// Регистрация нового пользователя
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Registration(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                return BadRequest(new BadRequestError {Description = "Ожидается email и password", Code = 101});
            }

            if (await _userManager.Users.AnyAsync(u => u.Email == email))
            {
                return BadRequest(new BadRequestError {Description = "Пользователь уже зарегистрирован", Code = 102});
            }

            var newUser = new User
            {
                Email = email
            };

            await _userManager.CreateAsync(newUser, password);
            await _userManager.AddToRoleAsync(newUser, Role.Client);

            return CreatedAtAction(nameof(GetProfile), new ProfileDto { Id = newUser.Id, Email = newUser.Email });
        }
    }
}