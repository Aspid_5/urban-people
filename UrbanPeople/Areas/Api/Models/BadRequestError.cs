namespace UrbanPeople.Areas.Api.Models
{
    public class BadRequestError
    {
        public int Code { get; set; }
        
        public string Description { get; set; }
    }
}