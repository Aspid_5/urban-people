namespace UrbanPeople.Areas.Api.Models.Dto
{
    public class PointDto
    {
        public string Id { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Number { get; set; }

        public string Name { get; set; }
    }
}