using UrbanPeople.Models;

namespace UrbanPeople.Areas.Api.Models.Dto
{
    public class TagDto
    {
        public string Id { get; set; }
        
        public string Title { get; set; }

        public TagDto(Tag tag)
        {
            Id = tag.Id;
            Title = tag.Title;
        }
    }
}