namespace UrbanPeople.Areas.Api.Models.Dto
{
    public class ProfileDto
    {
        public string Id { get; set; }

        public string Email { get; set; }
    }
}