using System.Collections.Generic;

namespace UrbanPeople.Areas.Api.Models.Dto
{
    public class RouteDto
    {
        public string Id { get; set; }

        public string OwnerId { get; set; }

        public string Name { get; set; }

        public List<PointDto> Points { get; set; }
    }
}