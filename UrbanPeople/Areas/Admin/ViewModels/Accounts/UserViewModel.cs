﻿using UrbanPeople.Models;

namespace UrbanPeople.Areas.Admin.ViewModels.Accounts
{
    public class UserViewModel
    {
        public User User { get; set; }
        public string UserRoles { get; set; }
        public string UserToken { get; set; }
    }
}