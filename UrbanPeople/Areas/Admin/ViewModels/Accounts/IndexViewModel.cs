using System.Collections.Generic;
using UrbanPeople.Models;

namespace UrbanPeople.Areas.Admin.ViewModels.Accounts
{
    public class IndexViewModel
    {
        public List<UserViewModel> UserList { get; set; }
    }
}