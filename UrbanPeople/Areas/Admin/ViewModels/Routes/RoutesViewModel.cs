﻿using System.Collections.Generic;
using UrbanPeople.Models;


namespace UrbanPeople.Areas.Admin.ViewModels.Routes
{
    public class RoutesViewModel
    {
        public IEnumerable<Route> Routes { get; set; }
    }
}