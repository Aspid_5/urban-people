﻿using UrbanPeople.Models;

namespace UrbanPeople.Areas.Admin.ViewModels.Routes
{
    public class RouteViewModel
    {
        public Route Route { get; set; }
    }
}