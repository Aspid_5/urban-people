﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Areas.Admin.ViewModels.Routes;
using UrbanPeople.Data;
using UrbanPeople.Models;

namespace UrbanPeople.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class RoutesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RoutesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET
        public async Task<IActionResult> Index()
        {
            // Получаем список маршрутов.
            var routes = await _context.Routes.ToListAsync();
            
            return View(new RoutesViewModel 
            {
                    Routes = routes
            });
        }
        
        [HttpGet]
        public async Task<IActionResult> Route(Route route)
        {
            return View(new RouteViewModel
            {
                Route = await _context.Routes
                    .Include(p => p.Points)
                    .Include(u => u.Owner)
                    .Where(record => record.Id == route.Id)
                    .SingleAsync()
            });
        }
    }
}