using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UrbanPeople.Areas.Admin.ViewModels.Accounts;
using UrbanPeople.Models;

namespace UrbanPeople.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class AccountsController : Controller
    {
        private readonly UserManager<User> _userManager;

        public AccountsController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        // GET
        public async Task<IActionResult> Index()
        {
            // Получаем список пользователей.
            var users = await _userManager.Users.ToListAsync();
            // Создаем список для пользователей и их ролей.
            var userList = new List<UserViewModel>();
            // Добавляем пользователей и их роли в список.
            foreach (var user in users)
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                userList.Add(new UserViewModel
                {
                    User = user,
                    UserRoles = string.Join(", ", userRoles)
                });
            }

            return View(new IndexViewModel
            {
                UserList = userList
            });
        }
    }
}