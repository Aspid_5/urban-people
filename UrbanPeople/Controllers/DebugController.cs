using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UrbanPeople.Authentication;
using UrbanPeople.Data;
using UrbanPeople.Models;

namespace UrbanPeople.Controllers
{
    [Authorize(Roles = "Admin, Developer")]
    public class DebugController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly UserManager<User> _userManager;

        public DebugController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Claims()
        {
            var options = new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore};
            return Ok(JsonConvert.SerializeObject(User.Claims, options));
        }

        /// <summary>
        /// Получение токена для доступа к API для текущего пользователя
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Security()
        {
            var user = await _userManager.FindByEmailAsync(User.Identity.Name);
            var token = await _userManager.GetAuthenticationTokenAsync(user, XAuthTokenHandler.TokenProvider,
                XAuthTokenHandler.TokenPurpose);

            if (string.IsNullOrEmpty(token))
            {
                return Json(new
                {
                    Error = "Token not exists"
                });
            }

            var base64Token = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{user.Id} : {token}"));

            return Json(new
            {
                UserEmail = user.Email,
                EncodedToken = base64Token
            });
        }
        
        /// <summary>
        /// Получение хэша пароля для указанного пользователя
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<IActionResult> Hash(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var hash = _userManager.PasswordHasher.HashPassword(user, password);
            
            return Json(new
            {
                UserEmail = user.Email,
                HashedPassword = hash
            });
        }

        /// <summary>
        /// Загрузка тестовых маршрутов
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> LoadRoutes()
        {
            var user = await _userManager.FindByEmailAsync("d.v.pakhomov@gmail.com");

            if (user != null)
            {
                var route =  new Route(user, "Первый маршрут");
                route
                    .AddPoint(new Point(51.659918, 39.210936))
                    .AddPoint(new Point(51.653344, 39.113097));

                _context.Routes.Add(route);
                await _context.SaveChangesAsync();

                return Json(new
                {
                    Status = "Success"
                });
            }

            return Json(new
            {
                Status = "Error"
            });
        }

        public async Task<IActionResult> LoadTags()
        {
            _context.AddRange(new List<Tag>
            {
                new Tag("Театры"),
                new Tag("Кино"),
                new Tag("Бары"),
                new Tag("Рестораны"),
                new Tag("Фестивали"),
            });

            await _context.SaveChangesAsync();

            return Json(new
            {
                Status = "Success"
            });
        }
    }
}